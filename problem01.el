;; -*- lexical-binding: t; -*-
(require 'json)
(require 'seq)

(defun parse-request (line)
  (ignore-errors
    (let* ((json-object-type 'plist)
           (json (json-read-from-string line)))
      (when (equal (plist-get json :method) "isPrime")
        (plist-get json :number)))))

(defun primep (n)
  (cond
   ((< n 2)
    nil)
   ((and (/= n 2) (zerop (% n 2)))
    nil)
   ((seq-some (lambda (x)
                (zerop (% n x)))
              (number-sequence 3 (sqrt n) 2))
    nil)
   (t t)))

(defun server-filter (process string)
  ;; (message "Received string (%s): %S" process string)
  (when (not (process-buffer process))
    (set-process-buffer process (generate-new-buffer "prime-time")))
  (let ((buf (process-buffer process)))
    (when (buffer-live-p buf)
      (with-current-buffer buf
        (save-excursion
          (goto-char (process-mark process))
          (insert string)
          (set-marker (process-mark process) (point)))
        ;; (message "Appended string (%s): %S" process string)
        (while (looking-at ".*\n")
          (let* ((line (buffer-substring (line-beginning-position)
                                         (line-end-position)))
                 (arg (parse-request line)))
            (if (numberp arg)
                (process-send-string
                 process
                 (concat
                  (json-encode
                   `(:method "isPrime"
                     :prime ,(or (primep (truncate arg)) :json-false)))
                  "\n"))
              (process-send-string
               process
               (concat (json-encode '(:error "malformed")) "\n"))
              (process-send-eof process)))
          (forward-line 1))))))

(defun server-sentinel (process event)
  ;; (message "Event (%s): %s" process event)
  (when (eq (process-status process) 'closed)
    (kill-buffer (process-buffer process))
    (delete-process process)))

;; (defun server-log (server connection message)
;;   (message "%s (%s): %s" server connection message))

(defvar server
  (make-network-process
   :name "prime-time"
   :server t
   :host "0.0.0.0"
   :service 10000
   :family 'ipv4
   :coding 'utf-8
   :filter #'server-filter
   :sentinel #'server-sentinel
   :log #'server-log))

(while t
  (accept-process-output nil 0.01))
