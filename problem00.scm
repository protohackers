(module problem00 ()

(import scheme)
(import (chicken base))
(import (chicken format))
(import (chicken process signal))
(import (chicken time))
(import (srfi 18))
(import socket)

(define stderr (current-error-port))
(define str string-append)

(define (elog fmt #!rest args)
  (apply fprintf stderr (str "[~s] " fmt) (current-process-milliseconds) args))

(define host "0.0.0.0")
(define port 10000)

(define max-data-size 1024)
(define backlog 10)
(socket-receive-timeout #f)

(define listener (socket af/inet sock/stream))
(set! (so-reuse-address? listener) #t)
(socket-bind listener (inet-address host port))
(socket-listen listener backlog)

(define (handle-client conn-socket)
  (elog "Incoming connection (~s)\n" conn-socket)
  (let loop ()
    (let ((data (socket-receive conn-socket max-data-size)))
      (if (zero? (string-length data))
          (socket-close conn-socket)
          (begin
            (elog "Received ~a bytes: ~s\n" (string-length data) data)
            (elog "Sent ~a bytes\n" (socket-send conn-socket data))
            (loop))))))

(let loop ()
  (let ((conn-socket (socket-accept listener)))
    (thread-start!
     (make-thread
      (lambda ()
        (handle-client conn-socket))))
    (loop)))

(set-signal-handler! signal/int (lambda (_) (socket-close listener) (exit 0)))

)
