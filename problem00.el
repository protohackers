;; -*- lexical-binding: t; -*-

(defun server-filter (process string)
  ;; (message "Received string (%s): %S" process string)
  (process-send-string process string))

(defun server-sentinel (process event)
  ;; (message "Event (%s): %s" process event)
  (when (eq (process-status process) 'closed)
    (delete-process process)))

;; (defun server-log (server connection message)
;;   (message "%s (%s): %s" server connection message))

(defvar server
  (make-network-process
   :name "echo"
   :server t
   :host "0.0.0.0"
   :service 10000
   :family 'ipv4
   :coding 'binary
   :filter #'server-filter
   :sentinel #'server-sentinel
   ;; :log #'server-log
   ))

(while t
  (accept-process-output nil 0.01))
