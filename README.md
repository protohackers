## About

Solutions for the [Protohackers] problems in both Emacs Lisp and
[CHICKEN] Scheme.

[Protohackers]: https://protohackers.com/
[CHICKEN]: https://call-cc.org/
